# pyren

Simply file renamer

Allows you to batch rename multiple files at once. Pyren offers common renaming procedures, such (insert, replace, remove blank spaces, add number sequences, etc). For advanced users, there is a Regex rule, which let users insert their regular expressions for renaming.

    usage: pyren.py [-h] [-i STR] [-a NUM] [-D] [-p POS][-r OLD NEW | -re 'RGX' NEW] [-v] [directory]

    Simply Renamer

    positional arguments:
      directory      Working directory (Default current directory)

    optional arguments:
      -h, --help     show this help message and exit
      -i STR         Inserts text
      -a NUM         Inserts autonumeric (Num: start number)
      -D             Inserts actual date to de files
      -p POS         Insertion Position (default: start)
      -r OLD NEW     Replace text in the file names
      -re 'RGX' NEW  Use regular expression to replace text
      -v, --version  show program's version number and exit


