#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
# pyren.py
#
# Another File Renammer
#
# Copyright (C) 2014 Alfredo Rodríguez
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function
import sys
import os
import re
import argparse
import datetime

__vmajor__ = 0
__vminor__ = 2
__vpatch__ = 1
__version__ = "{0}.{1}.{2}".format(__vmajor__, __vminor__, __vpatch__)
__author__ = "Alfredo Rodríguez"


def insertstr(orig, new, pos):
    """ Inserts str into another at given position """
    if pos > len(orig) or pos < 0:
        return orig + new
    else:
        return orig[:pos] + new + orig[pos:]


def getfilenames(directory):
    """ Return list with filenames in a directory """
    # Previous file list
    filenames = []
    for name in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, name)) and not name.startswith("."):
            filenames.append(name)
    if not filenames:
        print("There are no files in: %s", args.directory)
        raise SystemExit
    return filenames


def setfilenames(directory, oldfilenames, newfilenames):
    """ Rename files in a directory """
    for (old, new) in zip(oldfilenames, newfilenames):
        try:
            os.rename(os.path.join(args.directory, old),
                      os.path.join(args.directory, new))
        except OSError as e:
            print(e.strerror)
            raise SystemExit


def renamefile(name, args, autonum=None):
    """ Rename filename bassed on arguments passed """
    oldname, ext = os.path.splitext(name)
    newname = oldname
    if args.insert:
        text = args.insert
        newname = insertstr(newname, text, args.position)
    if args.date:
        d = datetime.date.today()
        newname = insertstr(newname, d.strftime("(%d-%m-%y)"), args.position)
    if autonum:
        newname = insertstr(newname, str(autonum), args.position)
    if args.replace:
        old, new = args.replace
        newname = newname.replace(old, new)
    elif args.regex:
        # reg is a valid pattern to use in a regular expression
        # count: max num of ocurrences to replace
        reg, new = args.regex
        newname = re.sub(reg, new, newname)

    return str(newname + ext)


def renamefiles(oldfilenames, args):
    """ Return a list with new filenames based on arguments passed """
    newfilenames = []
    autonum = args.autonum
    for name in oldfilenames:
        if autonum:
            newname = renamefile(name, args, autonum)
            autonum += 1
        else:
            newname = renamefile(name, args)
        # Add new file name to the new filenames list
        newfilenames.append(newname)
    return newfilenames


def getparser():
    parser = argparse.ArgumentParser(description="Simply Renamer",
                                     conflict_handler="resolve")
    parser.add_argument("directory", nargs="?", default=os.getcwd(),
                        help="Working directory (Default current directory)")
    parser.add_argument("-i", help="Inserts text",
                        metavar="STR", dest="insert")
    parser.add_argument("-a", default=None, type=int,
                        help="Inserts autonumeric (Num: start number)",
                        metavar="NUM", dest="autonum")
    parser.add_argument("-D", action="store_true", default=False,
                        help="Inserts actual date to de files", dest="date")
    parser.add_argument("-p", default=0, type=int,
                        help="Insertion Position (default: start)",
                        metavar="POS", dest="position")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-r", nargs=2, action="store",
                       help="Replace text in the file names",
                       metavar=("OLD", "NEW"), dest="replace")
    group.add_argument("-re", nargs=2,
                       help="Use regular expression to replace text",
                       metavar=("'RGX'", "NEW"), dest="regex")
    parser.add_argument("-v", "--version", action="version",
                        version="%(prog)s " + __version__)
    return parser

if __name__ == "__main__":
    # Python 2 and 3 compatible user input
    # If this is Python 2, use raw_input()
    if sys.version_info < (3, 0):
        input = raw_input

    # Argument parser
    parser = getparser()
    args = parser.parse_args()
    oldfilenames = getfilenames(args.directory)
    newfilenames = renamefiles(oldfilenames, args)
    # check if made some changes
    if oldfilenames == newfilenames:
        print("No changes!")
        raise SystemExit
    else:
        print("New file names:")
        for filename in newfilenames:
            print(filename)

    valid = {"yes": ["yes", "y", ""], "no": ["no", "n"]}
    while True:
        ch = input("Are you sure to apply changes (yes)?: ").lower()
        if ch in valid["yes"]:
            setfilenames(args.directory, newfilenames)
            print("Successfully renamed files")
            break
        elif ch in valid["no"]:
            print("Cancelled by user")
            break
        else:
            print("valid 'yes' or 'no'")
